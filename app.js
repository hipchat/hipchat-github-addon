if (process.env.NEW_RELIC_LICENSE_KEY) {
    require('newrelic');
}
var express = require('express');
var bodyParser = require('body-parser')
var compression = require('compression')
var morgan = require('morgan')
var ac = require('atlassian-connect-express');
process.env.PWD = process.env.PWD || process.cwd(); // Fix expiry on Windows :(
var expiry = require('static-expiry');
var hbs = require('express-hbs');
var path = require('path');
var os = require('os');
var crypto = require('crypto');

ac.store.register('redis', require('atlassian-connect-express-redis'));

var staticDir = path.join(__dirname, 'public');
var viewsDir = __dirname + '/views';
var routes = require('./routes');
var app = express();
var addon = ac(app, {
  config: {
    descriptorTransformer: (descriptor, config) => {
      if (process.env['ADDON_KEY']) {
        descriptor.key = `${descriptor.key}-${process.env['ADDON_KEY']}`
        descriptor.name = `${descriptor.name} (${process.env['ADDON_KEY']})`
      }
      return descriptor
    }
  }
});

addon.API_BASE_URI = 'https://api.github.com';

// Load the HipChat AC compat layer
var hipchat = require('atlassian-connect-express-hipchat')(addon, app);

// Github OAuth
var passport = require('passport');
var GitHubStrategy = require('passport-github').Strategy;

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  addon.settings.get(id, 'github').then(function(d){
    done(null, d);
  });
});

passport.use(new GitHubStrategy({
    clientID: process.env['GH_ID'],
    clientSecret: process.env['GH_SECRET'],
    callbackURL: addon.config.localBaseUrl() + "/auth/github/callback",
    userAgent: 'hipchat.com'
  },
  function(accessToken, refreshToken, user, done) {
    user.accessToken = accessToken;
    user.refreshToken = refreshToken;
    addon.settings.set(user.id, user, 'github').then(function(d){
      done(null, user);
    }).catch(function(err){
      done(err, user);
    });
  }
));

var port = addon.config.port();
var devEnv = app.get('env') == 'development';

app.set('port', port);

app.engine('hbs', hbs.express3({partialsDir: viewsDir}));
app.set('view engine', 'hbs');
app.set('views', viewsDir);

app.use(morgan(devEnv ? 'dev' : 'combined'))
app.use(compression());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({
  // FIXME hmac sig doesn't match
  verify: function (req, res, buffer){
    var providedSignature = req.headers['x-hub-signature'];
    if (!providedSignature) {
      return;
    }
    var hmac = crypto.createHmac('sha1', process.env['GH_SECRET']);
    hmac.update(buffer);
    var calculatedSignature = 'sha1=' + hmac.digest('hex');
    if (providedSignature !== calculatedSignature) {
      req.isSecure = false;
    } else {
      req.isSecure = true;
    }
  }
}));

app.use(passport.initialize());
addon.passport = passport;

app.use(addon.middleware());

app.use(expiry(app, {dir: staticDir, debug: devEnv}));
app.use(express.static(staticDir));

routes(app, addon);

app.listen(port, function(){
  console.log('Add-on server running at '+ (addon.config.localBaseUrl()||('http://' + (os.hostname()) + ':' + port)));
});
